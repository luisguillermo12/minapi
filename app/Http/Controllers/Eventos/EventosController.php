<?php

namespace App\Http\Controllers\Eventos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EventosApiRequest;

class EventosController extends Controller
{

  public function index()
  {
  return response()->json(['success'=>'true']);
  }


  public function eventosPost(EventosApiRequest $request)
  {
      $fromAux = $request->get('year');
      $yearNow = date("Y");
      $yearpass= 2014;

         if ($fromAux>$yearNow){
           return response()->json(['errors'=>['year'=>'year is greater than the current']]);
         }

         if ($fromAux<$yearpass){
           return response()->json(['errors'=>['year'=>'year not allowed']]);
         }
      $from  =  $fromAux."-01-01";
      $to  =   $fromAux."-12-31";


    $query = "SELECT activities.id AS id,
                activities.name AS nombre,
                activities.activity_category_id AS componente_id,
                activity_categories.name AS componente,
                activities.activity_subcategory_id AS subcomponente_id,
                IFNULL(activity_types.id, '') AS producto_id,
                IFNULL(activity_types.name, '') AS producto,
                IFNULL(organizers.id, '') AS alianzas_estrategica_id,
                IFNULL(organizers.name, '') AS alianzas_estrategica,
                IFNULL(activities.address, '') AS direccion,
                IFNULL(activities.description, '') AS descripcion,
                IFNULL(activities.address, '')AS direccion,IFNULL(regions.id, '') AS region_id,
                IFNULL(regions.name, '') AS region,
                IFNULL(provinces.id, '') AS provincia_id,IFNULL(provinces.code, '') AS cod_provincia,
                IFNULL(provinces.name, '') AS provincia,IFNULL(communes.id, '') AS comuna_id,
                IFNULL(communes.code, '') AS cod_comuna,IFNULL(communes.name, '') AS comuna,
                activities.start AS fec_inicio,
                activities.end AS fec_termino,
                IFNULL((SELECT GROUP_CONCAT(CONCAT(CASE WHEN day = 'L' THEN 'Lu' WHEN day = 'M' THEN 'Ma' WHEN day = 'X' THEN 'Mi' WHEN day = 'J' THEN 'Ju' WHEN day = 'V' THEN 'Vi' WHEN day = 'S' THEN 'Sa'WHEN day = 'D' THEN 'Do'END, ': ', start_time, '-', end_time) SEPARATOR ', ') FROM activity_times WHERE activity_times.activity_id = activities.id), '') AS horarios
                FROM  activities
                LEFT JOIN communes ON communes.id = activities.commune_id
                LEFT JOIN activity_categories ON activity_categories.id = activities.activity_category_id
                LEFT JOIN activity_types ON activity_types.id = activities.activity_type_id
                LEFT JOIN organizers ON organizers.id = activities.organizer_id
                LEFT JOIN regions ON regions.id = communes.region_id
                LEFT JOIN provinces ON provinces.id = communes.province_id
                JOIN activity_sessions ON activity_sessions.activity_id = activities.id
                WHERE
                activities.public = 1
                AND activities.deleted_at IS NULL
                AND activities.customer_id = 14
                AND activities.activity_module_id = 1
                and activity_types.name like '%evento%'
                and activity_categories.id  != 68
                and activities.created_at between '$from' and  '$to'
                and  activities.start >= '$from'
                GROUP BY activities.id
                ORDER BY activities.name ASC";

    $eventos = DB::select($query);
    return response()->json($eventos , 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
  }
}
