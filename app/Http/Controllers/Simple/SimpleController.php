<?php

namespace App\Http\Controllers\Simple;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EventosApiRequest;

class SimpleController extends Controller
{

  public function index()
  {
  return response()->json(['success'=>'true']);
  }


  public function tiposDocuemntosJson(Request $request)
  {
    $tiposDocumentos = ['id'=>[1, 2 , 3, 4] , 'titulo'=>['Resoluscion' , 'Oficio' , 'Decreto' , 'Ley']];
    return response()->json($tiposDocumentos , 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
      
  }


}
